# dcon-php
A Base PHP image for use by upstream web applications needing a quick way to get up and running in Docker with common packages included.


## Basic Information

### Tags
* latest [Dockerfile](Dockerfile)

### Included Packages
* php

### Features
* Default Docker ENTRYPOINT `run-cmd.sh`
* Pluggable script directory to extend the container `/_run/entrypoint`




### ENTRYPOINT
When building downstream images, you should use the same `_run` and `_build` folders to hold files used in building your images. The entrypoint will run scripts at runtime from the directory `/_run/entrypoint`.
Also do not use `ENTRYPOINT` in your container. It would override the entrypoint in the base container, which is used to inject the Oracle wallet credentials and install other scripts at container runtime.

### CMD
The default command for this image will start apache. Normally there is no need to change this.
