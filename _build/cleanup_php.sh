#!/bin/bash
set -euo pipefail

apt-get remove -y \
    libbz2-dev \
    libc-client-dev \
    libcurl4-openssl-dev \
    libfreetype6-dev \
    libgeoip-dev \
    libicu-dev \
    libjpeg62-turbo-dev \
    libkrb5-dev \
    libldap2-dev \
    libldb-dev \
    libmcrypt-dev \
    libmemcached-dev \
    libpq-dev \
    libsqlite3-dev \
    libssh2-1-dev \
    libssl-dev \
    libtidy-dev \
    libxml2-dev \
    libz-dev

# Clean up
rm -rf /tmp/*