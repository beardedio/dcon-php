#!/bin/bash
set -euo pipefail

# Work in tmp folder
cd /tmp/

# This script installs php composer
# For more info go to this URL
# https://getcomposer.org/doc/faqs/how-to-install-composer-programmatically.md
HSH=`wget -q -O - https://composer.github.io/installer.sig`
URL="https://getcomposer.org/installer"
PKG="composer-setup.php"

# Install git so we can require git repos
apt-get install -y \
    git \
    ssh \
    unzip \
    zip \
    --no-install-recommends

wget -nv -O "$PKG" "$URL"

echo "$HSH  $PKG" >> composer.hsh
echo "Checking hash of downloaded file:"
sha384sum "$PKG"
sha384sum -c composer.hsh

# Run composer installer
mkdir -p /usr/share/composer/
php composer-setup.php --install-dir=/usr/share/composer/

# Create a symbolic link
ln -s /usr/share/composer/composer.phar /usr/bin/composer

# Make sure we have a place to install composer
mkdir -p /opt/composer
export COMPOSER_HOME=/opt/composer
export PATH=$COMPOSER_HOME/vendor/bin:$PATH

# Include composer in root's path
echo 'export COMPOSER_ALLOW_SUPERUSER=1' >> /root/.bashrc
echo 'export COMPOSER_HOME=/opt/composer' >> /root/.bashrc
echo 'export PATH=$COMPOSER_HOME/vendor/bin:$PATH' >> /root/.bashrc

# Clean up
rm -rf /tmp/*
