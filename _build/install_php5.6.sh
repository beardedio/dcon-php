#!/bin/bash
set -euo pipefail

# PHP5 Only Config
docker-php-ext-install -j$(nproc) \
    mysql \
    mssql \
    mcrypt

pecl install \
    apcu-4.0.11 \
    geoip-1.1.1 \
    memcached-2.2.0 \
    redis-4.3.0 \
    ssh2-0.13 \
    xdebug-2.5.5

docker-php-ext-enable \
    apcu \
    geoip \
    memcached \
    redis \
    ssh2
    # Install but don't enable xdebug
