#!/bin/bash
set -euo pipefail

# PHP7 Only Config
pecl install \
    apcu \
    geoip-1.1.1 \
    mcrypt-1.0.2 \
    memcached \
    redis \
    ssh2-1.1.2 \
    xdebug

docker-php-ext-enable \
    apcu \
    geoip \
    mcrypt \
    memcached \
    redis \
    ssh2
    # Install but don't enable xdebug
