#!/bin/bash
set -euo pipefail

# Remove unneeded php config
php_ini_dir=/usr/local/etc/php/conf.d
if ([ ${SERVER_ENV:-null} == "dev" ] || [ ${SERVER_ENV:-null} == "stage" ]); then
    echo "Running php with DEV/STAGE settings"
    rm -f "$php_ini_dir/php_prod.ini"
else
    echo "Running php with PROD settings"
    rm -f "$php_ini_dir/php_nonprod.ini"
fi

if ([ ${SERVER_ENV:-null} == "dev" ] && [ ${XDEBUG:-null} == "true" ]); then
    echo "Running php with xdebug ENABLED"
    docker-php-ext-enable xdebug
else
    echo "Running php with xdebug DISABLED"
    #rm -f "$php_ini_dir/xdebug.ini"
fi