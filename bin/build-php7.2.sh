#!/bin/bash
set -euo pipefail

# Colorize me baby
green() { printf '\e[1;32m%b\e[0m\n' "$@"; }
yellow() { printf '\e[1;33m%b\e[0m\n' "$@"; }
red() { printf '\e[1;31m%b\e[0m\n' "$@"; }

# Change to base dir
cd "${0%/*}/.."

# Variables
CONTAINER=beardedio/dcon-php
TAG=debian-fpm-7.2

green "Build ${CONTAINER}:${TAG}"
docker build --pull -f Dockerfile-php7.2 -t "${CONTAINER}:${TAG}" .

if [[ "${@-}" == *push* ]]; then
    docker push "${CONTAINER}:${TAG}"
fi
